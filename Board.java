public class Board {
	
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	public Board () {
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new boolean[12];
	}
	public String toString() {
		
		String returnedString = " ";
		
		for(int i = 0; i < closedTiles.length; i++) {
			
			if(closedTiles[i]) {
				returnedString += (" " + (i+1) + " ");
			}
			else {
				returnedString += " X ";
			}	
		}
		return returnedString;
	}
	public boolean playATurn() {
		
		this.die1.roll();
		this.die2.roll();
		
		System.out.println("First roll: " + this.die1.getPips() + "\n");
		System.out.println("Second roll: " + this.die2.getPips() + "\n");
		
		int sum = this.die1.getPips() + this.die2.getPips();
		
		System.out.println("The Sum of both pips is " + sum);
		
		if(!(closedTiles[sum-1])) {
			
			System.out.println("Closing tile: " + sum + "\n");
			closedTiles[sum-1] = true;
			return false;
		}
		else {
			
			System.out.println("The tile at this position is already shut");
			return true;
		}
	}
}

