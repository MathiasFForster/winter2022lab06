public class ShutTheBox {
	
	public static void main(String[] args) { 
	
	System.out.println("Hi! Welcome to ShutTheBox\n");
	
	Board board = new Board();
	
	boolean gameOver = false;
	
	while(!gameOver) {
		System.out.println("Player 1'turn " + board + "\n");
		if(board.playATurn()) {
			System.out.println("Player 2 wins");
			gameOver = true;
		}
		else {
			System.out.println("Player 2'turn " + board + "\n");
			if(board.playATurn()) {
			 System.out.println("Player 1 wins");
			 gameOver = true;
			}
		}
	 }
  }
}
