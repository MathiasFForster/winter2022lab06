import java.util.Random;
public class Die {
	
	private int pips;
	private Random random;
	
	public Die() {
		this.pips = 1;
		this.random = new Random();
	}
	public int getPips() {
		return this.pips;
	}
	public Random getRandom() { 
		return this.random;
	}
	public void roll() {
		this.pips = this.random.nextInt((6-1) + 1) + 1;
	}
	public String toString() {
		return "The value of the pips is " + this.pips;
	}
}